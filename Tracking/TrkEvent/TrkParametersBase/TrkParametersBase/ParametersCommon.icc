/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

// Trk
#include "TrkEventPrimitives/ParamDefs.h"
// STD
#include <utility>

namespace Trk {
// Helper protected ctor
template <int DIM, class T>
inline ParametersCommon<DIM, T>::ParametersCommon(
    const AmgVector(DIM) parameters,
    std::optional<AmgSymMatrix(DIM)>&& covariance, const T chargeDef)
    : m_parameters(parameters),
      m_covariance(std::move(covariance)),
      m_chargeDef(chargeDef) {}

// Helper protected ctor
template <int DIM, class T>
inline ParametersCommon<DIM, T>::ParametersCommon(
    std::optional<AmgSymMatrix(DIM)>&& covariance)
    : m_parameters(),
    m_covariance(std::move(covariance)),
    m_chargeDef{} {}


// Protected Constructor with local arguments
template <int DIM, class T>
inline ParametersCommon<DIM, T>::ParametersCommon(
    const AmgVector(DIM) & parameters,
    std::optional<AmgSymMatrix(DIM)>&& covariance)
    : m_parameters(parameters),
      m_covariance(std::move(covariance)),
      m_chargeDef{} {}

template <int DIM, class T>
inline const AmgVector(DIM) & ParametersCommon<DIM, T>::parameters() const {
  return m_parameters;
}

template <int DIM, class T>
inline AmgVector(DIM) & ParametersCommon<DIM, T>::parameters() {
  return m_parameters;
}

template <int DIM, class T>
inline const AmgSymMatrix(DIM) *ParametersCommon<DIM, T>::covariance() const {
  if (m_covariance) {
    return &(*m_covariance);
  } else {
    return nullptr;
  }
}

template <int DIM, class T>
inline AmgSymMatrix(DIM) * ParametersCommon<DIM, T>::covariance() {
  if (m_covariance) {
    return &(*m_covariance);
  } else {
    return nullptr;
  }
}

template <int DIM, class T>
inline double ParametersCommon<DIM, T>::pT() const {
  return momentum().perp();
}

template <int DIM, class T>
inline double ParametersCommon<DIM, T>::eta() const {
  return momentum().eta();
}

template <int DIM, class T>
inline constexpr bool ParametersCommon<DIM, T>::isCharged() const {
  if constexpr (std::is_same<T, Trk::Neutral>::value) {
    return false;
  } else {
    return true;
  }
}

template <int DIM, class T>
inline Amg::Vector2D ParametersCommon<DIM, T>::localPosition() const {
  return Amg::Vector2D(m_parameters[Trk::loc1], m_parameters[Trk::loc2]);
}

template <int DIM, class T>
inline void ParametersCommon<DIM, T>::setParameters(
  const AmgVector(DIM) & param) {
  m_parameters = param;
}

template <int DIM, class T>
inline void ParametersCommon<DIM, T>::setCovariance(
  const AmgSymMatrix(DIM) & cov) {
  m_covariance = cov;
}

template <int DIM, class T>
inline void ParametersCommon<DIM, T>::updateParameters(
  const AmgVector(DIM)& updatedParameters) {
  this->updateParametersHelper(updatedParameters);
}

template <int DIM, class T>
inline void ParametersCommon<DIM, T>::updateParameters(
  const AmgVector(DIM) &updatedParameters,
  const AmgSymMatrix(DIM) & updatedCovariance) {
  m_covariance = updatedCovariance;
  this->updateParametersHelper(updatedParameters);
}

}  // end of namespace Trk

